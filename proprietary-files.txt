### FIRMWARES
vendor/firmware/bcm4350_V0301.0562.hcd
vendor/firmware/bcm4350_V0301.0563_wisol.hcd
vendor/firmware/companion_2p2_master_setfile.bin
vendor/firmware/companion_2p2_mode_setfile.bin
vendor/firmware/companion_default_coef.bin
vendor/firmware/companion_default_lsc.bin
vendor/firmware/companion_fw_2p2_evt1.bin
vendor/firmware/companion_fw_evt0.bin
vendor/firmware/fimc_is_fw2.bin
vendor/firmware/fimc_is_fw2_2p2.bin
vendor/firmware/libpn547_fw.so
vendor/firmware/mfc_fw.bin
vendor/firmware/setfile_2p2.bin
vendor/firmware/setfile_6b2.bin
vendor/firmware/setfile_8b1.bin
etc/firmware/florida-dsp1-edac.bin
etc/firmware/florida-dsp1-edac.wmfw
etc/firmware/florida-dsp2-tx-nb.wmfw
etc/firmware/florida-dsp2-tx-swb.wmfw
etc/firmware/florida-dsp2-tx-wb.wmfw
etc/firmware/florida-dsp3-ez2-control.wmfw
etc/firmware/florida-dsp3-lpsd-control.bin
etc/firmware/florida-dsp3-tx-nb.bin
etc/firmware/florida-dsp3-tx-nb.wmfw
etc/firmware/florida-dsp3-tx-swb.wmfw
etc/firmware/florida-dsp3-tx-swb-conversation.bin
etc/firmware/florida-dsp3-tx-swb-interview.bin
etc/firmware/florida-dsp3-tx-wb.bin
etc/firmware/florida-dsp3-tx-wb.wmfw
etc/firmware/florida-dsp4-rx-anc-nb.bin
etc/firmware/florida-dsp4-rx-anc-nb.wmfw
etc/firmware/florida-dsp4-rx-anc-wb.bin
etc/firmware/florida-dsp4-rx-anc-wb.wmfw

### Wifi
etc/wifi/bcmdhd_apsta.bin
etc/wifi/bcmdhd_ibss.bin
etc/wifi/bcmdhd_mfg.bin
etc/wifi/bcmdhd_sta.bin
etc/wifi/cred.conf
etc/wifi/nvram_mfg.txt
etc/wifi/nvram_mfg.txt_semco3rd
etc/wifi/nvram_mfg.txt_wisol
etc/wifi/nvram_net.txt
etc/wifi/nvram_net.txt_semco3rd
etc/wifi/nvram_net.txt_wisol

#Bluetooth
lib/libbt-aptx-4.0.3.so
lib/libbt-codec_aptx.so
lib/libbt-iopdb.so
lib/libbt-iopdb_mod.so
lib/libbt-vendor.so

### Media (OMX)
lib/omx/libOMX.Exynos.AVC.Decoder.so
lib/omx/libOMX.Exynos.AVC.Encoder.so
lib/omx/libOMX.Exynos.MPEG4.Decoder.so
lib/omx/libOMX.Exynos.MPEG4.Encoder.so
lib/omx/libOMX.Exynos.VP8.Decoder.so
lib/omx/libOMX.Exynos.VP8.Encoder.so
lib/omx/libOMX.Exynos.WMV.Decoder.so

### Audio
lib/libmysound.so
lib/libaudiosa.so
lib/soundfx/libaudiosa_sec.so
vendor/lib/liblvverx_3.21.13.so
vendor/lib/liblvvetx_3.21.13.so

### Camera
lib/hw/camera.universal5422.so
lib/libarccamera.so
lib/libarcsoft_magicshot_bestface_T.so
lib/libarcsoft_magicshot_bestphoto_T.so
lib/libarcsoft_magicshot_common_T.so
lib/libarcsoft_magicshot_drama_T.so
lib/libarcsoft_magicshot_eraser_T.so
lib/libarcsoft_magicshot_picmotion_T.so
lib/libarcsoft_night_shot_ex.so
lib/libarcsoft_panorama_burstcapture.so
lib/libarcsoft_picaction.so
lib/libarcsoft_picmotion_T.so
lib/libarcsoft_preprocessor_T.so
lib/libarcsoft_readengine_T.so
lib/libarcsoft_selfie_camera_lite.so
lib/libarcsoft_sensorndk.so
lib/libarcsoft_vdoinvideo.so
lib/libexynoscamera.so
lib/libvdis.so

### BIN
bin/drsd
bin/exyrngd
bin/gps.cer
bin/gpsd
bin/npsmobex
bin/rild
bin/sensorhubservice
bin/vcsFPService
#bin/clatd
#etc/clatd.conf

### GPS
lib/hw/gps.universal5422.so

### HWC
vendor/lib/egl/libGLES_mali.so

### Sensors
lib/hw/sensors.universal5422.so

### Widevine
lib/drm/libdrmwvmplugin.so
vendor/lib/mediadrm/libwvdrmengine.so
vendor/lib/libwvdrm_L1.so
vendor/lib/libwvm.so
vendor/lib/libWVStreamControlAPI_L1.so

### Android radio daemon
lib/libaudio-ril.so
lib/libreference-ril.so
lib/libril.so
lib/librilutils.so
lib/libsecnativefeature.so
lib/libsec-ril.so

### MobiCore

app/FFFFFFFF000000000000000000000001.drbin
app/mcRegistry/00060308060501020000000000000000.tlbin
app/mcRegistry/02010000080300030000000000000000.tlbin
app/mcRegistry/07010000000000000000000000000000.tlbin
app/mcRegistry/07060000000000000000000000000000.tlbin
app/mcRegistry/ffffffff00000000000000000000000a.tlbin
app/mcRegistry/ffffffff00000000000000000000000c.tlbin
app/mcRegistry/ffffffff00000000000000000000000d.tlbin
app/mcRegistry/ffffffff00000000000000000000000e.tlbin
app/mcRegistry/ffffffff00000000000000000000000f.tlbin
app/mcRegistry/ffffffff000000000000000000000003.tlbin
app/mcRegistry/ffffffff000000000000000000000004.tlbin
app/mcRegistry/ffffffff000000000000000000000005.tlbin
app/mcRegistry/ffffffff000000000000000000000007.tlbin
app/mcRegistry/ffffffff000000000000000000000008.tlbin
app/mcRegistry/ffffffff000000000000000000000009.tlbin
app/mcRegistry/ffffffff000000000000000000000011.tlbin
app/mcRegistry/ffffffff000000000000000000000012.tlbin
app/mcRegistry/ffffffff000000000000000000000013.tlbin
app/mcRegistry/ffffffff000000000000000000000016.tlbin
app/mcRegistry/ffffffff000000000000000000000017.tlbin
app/mcRegistry/ffffffff000000000000000000000018.tlbin
app/mcRegistry/ffffffffd0000000000000000000000a.tlbin
app/mcRegistry/ffffffffd0000000000000000000000e.tlbin
app/mcRegistry/ffffffffd00000000000000000000004.tlbin
app/mcRegistry/ffffffffd00000000000000000000016.tlbin
bin/scranton_RD

# no arranca
vendor/lib/libmalicore.bc
vendor/lib/libRSDriverArm.so

lib/libexynosv4l2.so
lib/libhr.so
bin/mcDriverDaemon
bin/jackservice
bin/lpm
lib/hw/power.exynos5.so
bin/olsrd

bin/wlandutservice



















